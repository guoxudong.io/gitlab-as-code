terraform {
  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "15.8.0"
    }
  }
}


variable "gitlab_access_token" {
  type = string
}

provider "gitlab" {
  token = var.gitlab_access_token
}

data "gitlab_group" "sample_group" {
  group_id = 12322981
}

# Add a project to the group - guoxudong.io/example-create-by-tf
resource "gitlab_project" "sample_group_project" {
  name             = "Example Create by TF"
  namespace_id     = data.gitlab_group.sample_group.group_id
  visibility_level = "public"
}

# Add a variable to the project
resource "gitlab_project_variable" "sample_project_variable" {
  project = gitlab_project.sample_group_project.id
  key     = "example_variable"
  value   = "Greetings Master!"
}

# Add a label to the project
resource "gitlab_label" "fixme" {
  project     = gitlab_project.sample_group_project.id
  name        = "fixme"
  description = "issue with failing tests, create by terraform"
  color       = "#ffcc00"
}

# Add a label to the project
resource "gitlab_label" "bug" {
  project     = gitlab_project.sample_group_project.id
  name        = "bug"
  description = "bug label, create by terraform"
  color       = "#ff0000"
}