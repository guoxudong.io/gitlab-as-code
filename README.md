# GitLab as Code

- Create a GitLab project
- Add [personal access token](https://gitlab.com/-/profile/personal_access_tokens) to CI/CD Variables `TF_VAR_gitlab_access_token`
- Import CI template `Terraform.gitlab-ci.yml` to `.gitlab-ci.yml`
- (Optional) Add the `destroy` job to `.gitlab-ci.yml`
- Add `.gitignore` file
- Add `backend.tf` file for GitLab-managed Terraform state
- Add `main.tf`
- Commit & push to gitlab

> :warning: If the `fmt` Job fails, please run the `terraform fmt` command in your project. This does not affect the normal use of Terraform.
